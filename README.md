# Merlin Hotel Calendar #

View the whole years prices in one go!

### Data source ###

Use pre-cached data from http://merlinhotelcalendar.com/getData.php with the following get vars:
hotel - The name of the hotel (supports Safari, Azteca, Glamping, Alton, Woodland, Treehouse, Splash, Castle, Resort, Shark, Tower, Knight, Mediaeval)
nights - Number 1-5 since multiple nights have an unpredictable discount rate

Example:
http://www.merlinhotelcalendar.com/getData.php?hotel=Safari&nights=1
