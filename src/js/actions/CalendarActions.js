import dispatcher from "../app/dispatcher"

export const SELECTION_CHANGE = "SELECTION_CHANGE";

export function changeSelection(hotel, nights) {
  dispatcher.dispatch({
    type: SELECTION_CHANGE,
    hotel: hotel,
    nights: nights
  });
}
