import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, IndexRoute, hashHistory} from "react-router";

import Navigation from "../components/Navigation";
import Calendar from "../pages/Calendar";

const app = document.getElementById('app');
ReactDOM.render(<Router history={hashHistory}>
  <Route path="/(:hotel)(/:nights)" component={Navigation}>
    <IndexRoute component={Calendar}></IndexRoute>
  </Route>
</Router>, app);
