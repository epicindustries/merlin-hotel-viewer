import React from "react";

export default class Day extends React.Component {
  constructor(props) {
    super(props);

    this.price = this.props.price;
    this.styleClass = this.getClass(false);

    this.state = { class: this.styleClass, price: this.getDisplayPrice(this.price) };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.price != nextProps.price) {
      this.price = nextProps.price;
      this.styleClass = this.getClass(false);
      this.updateState();
    }
  }

  over() {
    this.styleClass = this.getClass(true);
    this.updateState();

  }

  out() {
    this.styleClass = this.getClass(false);
    this.updateState();
  }

  getClass(over) {
    return this.price ? "available " + (!over || "over") : "unavailable";
  }

  getDisplayPrice(price) {
    return price ? price.toLocaleString('en-GB', {
      style: 'currency',
      currency: 'GBP',
      maximumFractionDigits: 0
    }) : "N/A";
  }

  updateState() {
    this.setState({
      class: this.styleClass,
      price: this.getDisplayPrice(this.price)
    });
  }

  render() {

    return (
      <td class={this.state.class} onMouseOver={this.over.bind(this)} onMouseOut={this.out.bind(this)}>
        {this.props.date}<br />{this.state.price}
      </td>
    );
  }
}


