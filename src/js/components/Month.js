import React from "react";

import Day from "./Day";

export default class Month extends React.Component {

  static monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  static monthLengths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  constructor(props) {
    super(props);

    this.monthName = Month.monthNames[this.props.id];

    this.createWeeks(this.props.prices);

    this.state = { weeks: this.weeks };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.prices != nextProps.prices) {
      this.createWeeks(nextProps.prices);

      this.setState({
        weeks: this.weeks
      });
    }
  }

  createWeeks(prices) {
    this.weeks = [];

    let date = 1;
    while (true) {

      let week = [];
      let price = "";

      for (let day = 0; day < 7; day++) {
        if (date > 31) break;
        let price = prices[date];
        week.push(<Day key={date} date={date} price={price}/>);
        date++;
      }

      this.weeks.push(<tr key={date}>
        {week}
      </tr>);

      if (date > Month.monthLengths[this.props.id]) break;
    }
  }

  render() {
    return (
      <table class="calendar">
        <tbody>
        <tr class="currentmonth">
          <th colSpan="7">{this.monthName} 2017</th>
        </tr>
        <tr>
          <td class="weekdays">Sun</td>
          <td class="weekdays">Mon</td>
          <td class="weekdays">Tues</td>
          <td class="weekdays">Wed</td>
          <td class="weekdays">Thurs</td>
          <td class="weekdays">Fri</td>
          <td class="weekdays">Sat</td>
        </tr>
        {this.state.weeks}
        </tbody>
      </table>
    );
  }
}
