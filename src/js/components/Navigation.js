import React from "react";
import _ from "lodash"

import CalendarStore from "../stores/CalendarStore";
import * as CalendarActions from "../actions/CalendarActions";

export default class Navigation extends React.Component {

  constructor() {
    super();

    this.state = {
      hotel: "default",
      nights: 1
    };

    this.selectedHotel = "default";
    this.selectedNights = 1;

    this.hotelData = [
      { "id": "Safari", "name": "Chessington Safari Hotel" },
      { "id": "Azteca", "name": "Chessington Azteca Hotel" },
      { "id": "Glamping", "name": "Chessington Glamping" },
      { "id": "Alton", "name": "Alton Towers Hotel" },
      { "id": "Woodland", "name": "Alton Towers Woodland Lodges" },
      { "id": "Treehouse", "name": "Alton Towers Treehouse" },
      { "id": "Splash", "name": "Splash Landings Hotel" },
      { "id": "Castle", "name": "Legoland Castle Hotel" },
      { "id": "Resort", "name": "Legoland Resort Hotel" },
      { "id": "Shark", "name": "Thorpe Shark Hotel" },
      { "id": "Tower", "name": "Warwick Castle Tower Suite" },
      { "id": "Knight", "name": "Warwick Castle Knight's Village" },
      { "id": "Mediaeval", "name": "Warwick Castle Mediaeval Glamping" }
    ];

    this.hotels = [];
    this.nights = [];

    _.each(this.hotelData, (hotel)=> {
      this.hotels.push(<option key={hotel.id} value={hotel.id}>{hotel.name}</option>)
    });

    _.each([1, 2, 3, 4, 5], (nights)=> {
      this.nights.push(<option key={nights} value={nights}>{nights}</option>)
    });

    this.updateFromStore = this.updateFromStore.bind(this);
  }

  componentWillMount() {
    CalendarStore.on(CalendarStore.CHANGE, this.updateFromStore);
  }

  componentDidMount() {

    if (this.props.params.hotel) {
      this.selectedHotel = this.props.params.hotel;
    }
    if (this.props.params.nights) {
      this.selectedNights = this.props.params.nights;
    }

    this.updateState();

    if (this.selectedHotel != "default") {
      CalendarActions.changeSelection(this.selectedHotel, this.selectedNights);
    }
  }

  componentWillUnmount() {
    CalendarStore.removeListener(CalendarStore.CHANGE, this.updateFromStore);
  }

  updateFromStore() {
    this.selectedHotel = CalendarStore.hotel;
    this.selectedNights = CalendarStore.nights;
    this.updateState();
  }

  changeHotel(e) {
    this.selectedHotel = e.target.value;
    this.updateState();
  }

  changeNights(e) {
    this.selectedNights = e.target.value;
    this.updateState();
  }

  updateCalendar(e) {
    this.props.history.pushState(null, "/" + this.selectedHotel + "/" + this.selectedNights);

    CalendarActions.changeSelection(this.selectedHotel, this.selectedNights);

    e.preventDefault();
  }

  updateState() {
    this.setState({
      hotel: this.selectedHotel,
      nights: this.selectedNights
    });
  }

  render() {
    return (
      <div>
        <div id="header">
          <h1><img id="title" src="img/logo.png" alt="Merlin Hotel Calendar"/></h1>
          <div id="hotelSelect">
            <form>
              <select id="hotel" name="hotel" value={this.state.hotel}
                      onChange={this.changeHotel.bind(this)}>
                <option value="default" disabled="true">-- Select Hotel --</option>
                {this.hotels}
              </select>
              <select id="nights" name="nights" value={this.state.nights}
                      onChange={this.changeNights.bind(this)}>
                {this.nights}
              </select>
              <input type="submit" value="Submit" onClick={this.updateCalendar.bind(this)}/>
            </form>
          </div>
        </div>
        {this.props.children}
      </div>
    );
  }
}
