import React from "react";

import Month from "../Components/Month";
import CalendarStore from "../stores/CalendarStore";

export default class Calendar extends React.Component {

  constructor(props) {
    super(props);

    this.state = { months: [] };

    this.onDataLoaded = this.onDataLoaded.bind(this);
  }

  componentWillMount() {
    CalendarStore.on(CalendarStore.DATA_LOADED, this.onDataLoaded.bind(this));
  }

  componentsWillUnMount() {
    CalendarStore.removeListener(CalendarStore.DATA_LOADED, this.onDataLoaded);
  }

  onDataLoaded(data) {
    let prices = JSON.parse(data).prices;

    this.months = [];

    for (let month = 0; month < 12; month++) {
      this.months.push(<Month key={month} id={month} prices={prices[month]}/>);
    }

    this.setState({ months: this.months });
  }

  render() {
    return (
      <div>
        <div>
          {this.state.months}
        </div>
      </div>);
  }
}
