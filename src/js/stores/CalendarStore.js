import { EventEmitter } from "events";
import $ from "jquery";

import dispatcher from "../app/dispatcher";
import * as CalendarActions from "../actions/CalendarActions";

class CalendarStore extends EventEmitter {

  constructor() {
    super();

    this.CHANGE = "CHANGE";
    this.DATA_LOADED = "DATA_LOADED";

    this.hotel = "default";
    this.nights = 1;
  }

  updateSelection(hotel, nights) {

    this.hotel = hotel;
    this.nights = nights;

    this.emit(this.CHANGE);

    this.loadData();
  }


  loadData() {
    var hotel = this.hotel;
    var nights = this.nights;

    $.get(`http://www.merlinhotelcalendar.com/getData.php?hotel=${hotel}&nights=${nights}`, null, this.onDataLoaded.bind(this));
  };

  onDataLoaded(data) {
    this.emit(this.DATA_LOADED, data);
  }

  handleActions(action) {
    switch (action.type) {
      case CalendarActions.SELECTION_CHANGE:
        this.updateSelection(action.hotel, action.nights);
        break;
    }
  }
}

const calendarStore = new CalendarStore;
dispatcher.register(calendarStore.handleActions.bind(calendarStore));

export default calendarStore;
